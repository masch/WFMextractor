
WFMextractor
============

Tool to extract the measurement data and configuration
settings out of a Rigol DS1000E wave format file (*.wfm).

The measurement data can be stored in CSV and/or RIFF-WAVE
file format for further processing.

Along with the CSV file, a gnuplot script file can be generated
to be passed to gnuplot to draw a graph which looks similarly
to the oscilloscope display output.

Finally a text file can be generated that lists the oscilloscope
settings stored additionally in the measurement file.


Invocation
----------
    WFMextractor -c -g -t -v -w *.wfm
    -c     Write measurement data into CSV file.
    -g     Generate gnuplot script file.
    -t     Output oscilloscope settings into text file.
    -v     Print program version to stdout.
    -w     Write measurement data into RIFF-WAVE file.
    *.wfm  Rigol DS1000E oscilloscope waveform file(s).


Example usage
-------------
- **Build program:** `gcc -O3 -Wall -Wextra -o WFMextractor WFMextractor.c`
- **Generate source code documentation:** `doxygen WFMextractor.doxyfile`
- **Generate input files for gnuplot:** `WFMextractor -cg Probe_compensation.wfm`
- **Generate gnuplot output:** `gnuplot Probe_compensation.gp`


Credits
-------
WFMextractor was inspired by and benefits especially from the work of
Matthias Blaicher ([wfmutil](https://github.com/mabl/pyRigolWFM)) and Eric Gregg
([wfm2gif](https://github.com/hank/life/tree/master/code/c/rigol_wfm_reader)).
