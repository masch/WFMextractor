# ###################
# Gnuplot script file
# ###################
#
# Example settings for *.eps diagram
# set title "Probe_compensation.eps" font "Sans,20"
# set terminal epscairo font "Sans,16" fontscale 0.5 linewidth 3 size 10inch,8inch
# set output "Probe_compensation.eps"
# test
#
# Example settings for *.pdf diagram
# set title "Probe_compensation.pdf" font "Sans,20"
# set terminal pdfcairo font "Sans,16" fontscale 0.5 linewidth 3 size 10inch,8inch
# set output "Probe_compensation.pdf"
# test
#
# Example settings for *.png diagram
# set title "Probe_compensation.png" font "Sans,20"
# set terminal pngcairo font "Sans,16" fontscale 1.0 size 1280,1024
# set output "Probe_compensation.png"
# test
#
# Example settings for *.svg diagram
set title "Probe_compensation.svg" font "Sans,20"
set terminal svg fname "Sans" fsize 16 background "white" size 1280,1024 dynamic
set output "Probe_compensation.svg"
# test
#
# Enable grid lines
set grid back
#
# Set data separator to ',' because of *.csv data file
set datafile separator ","
#
# Horizontal scaling
set xlabel "Time [µs]" font "Sans,16"
set xrange [-3000:3000]
set xtics out 500 font "Sans,14" nomirror
#
# Vertical scaling CH1
set ylabel "Channel 1 [V]" font "Sans,16"
set yrange [-6:4]
set ytics out 1 font "Sans,14" nomirror
#
# Vertical scaling CH2
set y2label "Channel 2 [V]" font "Sans,16"
set y2range [-1:9]
set y2tics out 1 font "Sans,14" nomirror
#
# Trigger level and time instant CH1
set arrow from 0,-6 to 0,4 size character 1.0,150 filled front
set arrow from -3000,1.56 to 3000,1.56 nohead front linetype 0 linecolor 1
#
# Enable legend
# set key inside right top horizontal Left font "Sans,16"
set key inside right top vertical font "Sans,16"
#
# Plot diagram
plot "Probe_compensation.csv" skip 2 using ($1*1000000):($2*1) with lines linecolor 1 axis x1y1 title "CH1", \
     "Probe_compensation.csv" skip 2 using ($1*1000000):($3*1) with lines linecolor 2 axis x1y2 title "CH2"
