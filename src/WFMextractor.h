/**
 * @file
 *
 * @code
 * Program name   : WFMextractor
 *
 * Author         : Marcel Schlottmann
 *
 * License        : GPL
 *
 * Last change    : 2016-08-16
 * @endcode
 * @n
 */

#ifndef WFMEXTRACTOR_H_
#define WFMEXTRACTOR_H_

#include <stdint.h>

/* Structure of Rigol DS1000E/D WFM file header
 * gcc __attribute__((packed)) is used because of usage of uint64_t
 */
typedef struct __attribute__((packed)){
	/* General system parameters */
	uint16_t FileID;          // Offset 00h : Magic number (0xA5A5 expected)
	uint8_t  wfm_02h[18];     // Offset 02h
	uint32_t RollStpPt;       // Offset 14h : Roll mode stop point (0 .. [PtsPerCh-1])
	uint8_t  wfm_18h[4];      // Offset 18h
	uint32_t PtsPerCh;        // Offset 1Ch : Data points per channel (8k, 16k, 512k, 1M)
	uint8_t  ActiveCh;        // Offset 20h : Active channel (0=None, 1=CH1, 2=CH2, 3=REF, 4=MATH, 5=LA)
	uint8_t  wfm_21h[3];      // Offset 21h
	/* Channel 1 parameters */
	uint32_t vScaleCh1D;      // Offset 24h : Vertical scale CH1 display [µV/Div]
	int16_t  vPosCh1D;        // Offset 28h : Vertical position CH1 display [25Pts/Div]
	uint8_t  wfm_2Ah[4];      // Offset 2Ah
	uint16_t ProbeCh1;        // Offset 2Eh : Probe attenuation CH1 (3F80h=1X, 4120h=10X)
	uint8_t  DispCh1Inv;      // Offset 30h : Display data CH1 inverted (1=Y, 0=N)
	uint8_t  CH1data;         // Offset 31h : Channel 1 data stored (1=Y, 0=N)
	uint8_t  MemCh1Inv;       // Offset 32h : Memory data CH1 inverted (1=Y, 0=N)
	uint8_t  wfm_33h[1];      // Offset 33h
	uint32_t vScaleCh1M;      // Offset 34h : Vertical scale CH1 memory [µV/Div]
	int16_t  vPosCh1M;        // Offset 38h : Vertical position CH1 memory [25Pts/Div]
	uint8_t  wfm_3Ah[2];      // Offset 3Ah
	/* Channel 2 parameters */
	uint32_t vScaleCh2D;      // Offset 3Ch : Vertical scale CH2 display [µV/Div]
	int16_t  vPosCh2D;        // Offset 40h : Vertical position CH2 display [25Pts/Div]
	uint8_t  wfm_42h[4];      // Offset 42h
	uint16_t ProbeCh2;        // Offset 46h : Probe attenuation CH2 (3F80h=1X, 4120h=10X)
	uint8_t  DispCh2Inv;      // Offset 48h : Display data CH2 inverted (1=Y, 0=N)
	uint8_t  CH2data;         // Offset 49h : Channel 2 data stored (1=Y, 0=N)
	uint8_t  MemCh2Inv;       // Offset 4Ah : Memory data CH2 inverted (1=Y, 0=N)
	uint8_t  wfm_4Bh[1];      // Offset 4Bh
	uint32_t vScaleCh2M;      // Offset 4Ch : Vertical scale CH2 display [µV/Div]
	int16_t  vPosCh2M;        // Offset 50h : Vertical position CH2 memory [25Pts/Div]
	/* General time base parameters */
	uint8_t DelayedScan;      // Offset 52h : Delayed scan mode (1=On, 0=Off)
	uint8_t wfm_53h[1];       // Offset 53h
	// Time base 1 parameters
	uint64_t hScaleDT1;       // Offset 54h : Horizontal scale display time 1 [ps/Div]
	int64_t hPosDT1;          // Offset 5Ch : Horizontal position display time 1 [ps/Div]
	float SampleRateT1;       // Offset 64h : Sample rate time base 1 [Hz]
	uint64_t hScaleMT1;       // Offset 68h : Horizontal scale memory time 1 [ps/Div]
	int64_t hPosMT1;          // Offset 70h : Horizontal position memory time 1 [ps/Div]
	// Logic analyzer parameters (not tested yet)
	uint8_t LA1data;          // Offset 78h : Logic analyzer data group D7-D0 stored (1=Y, 0=N)
	uint8_t LA2data;          // Offset 79h : Logic analyzer data group D15-D8 stored (1=Y, 0=N)
	uint16_t LAchannels;      // Offset 7Ah : Logic analyzer channels (1=On, 0=Off, 1 bit per ch.)
	uint8_t vPosLaD0;         // Offset 7Ch : Vertical position LA channel D0
	uint8_t vPosLaD1;         // Offset 7Dh : Vertical position LA channel D1
	uint8_t vPosLaD2;         // Offset 7Eh : Vertical position LA channel D2
	uint8_t vPosLaD3;         // Offset 7Fh : Vertical position LA channel D3
	uint8_t vPosLaD4;         // Offset 80h : Vertical position LA channel D4
	uint8_t vPosLaD5;         // Offset 81h : Vertical position LA channel D5
	uint8_t vPosLaD6;         // Offset 82h : Vertical position LA channel D6
	uint8_t vPosLaD7;         // Offset 83h : Vertical position LA channel D7
	uint8_t vPosLaD8;         // Offset 84h : Vertical position LA channel D8
	uint8_t vPosLaD9;         // Offset 85h : Vertical position LA channel D9
	uint8_t vPosLaD10;        // Offset 86h : Vertical position LA channel D10
	uint8_t vPosLaD11;        // Offset 87h : Vertical position LA channel D11
	uint8_t vPosLaD12;        // Offset 88h : Vertical position LA channel D12
	uint8_t vPosLaD13;        // Offset 89h : Vertical position LA channel D13
	uint8_t vPosLaD14;        // Offset 8Ah : Vertical position LA channel D14
	uint8_t vPosLaD15;        // Offset 8Bh : Vertical position LA channel D15
	uint8_t DispSizeGrpLA1;   // Offset 8Ch : Display size LA group D7-D0 (7=Small, 15=Large)
	uint8_t DispSizeGrpLA2;   // Offset 8Dh : Display size LA group D15-D8 (7=Small, 15=Large)
	// General trigger parameters
	uint8_t TriggerMode;      // Offset 8Eh : Trigger system mode (0=Edge, 1=Pulse, 2=Slope, 3=Video, 4=Alternate, 5=Pattern, 6=Duration)
	// Trigger 1 parameters
	uint8_t ModeTr1;          // Offset 8Fh : Mode trigger 1 (0=Edge, 1=Pulse, 2=Slope, 3=Video)
	uint8_t SourceTr1;        // Offset 90h : Source trigger 1 (0=CH1, 1=CH2, 2=EXT, 3=AC Line, 4=D15-D0)
	uint8_t CouplingTr1;      // Offset 91h : Coupling trigger 1 (0=DC, 1=LF Reject, 2=HF Reject, 3=AC)
	uint8_t SweepTr1;         // Offset 92h : Sweep trigger 1 (0=Auto, 1=Normal, 2=Single)
	uint8_t wfm_93h[1];       // Offset 93h
	float SensitivityTr1;     // Offset 94h : Sensitivity trigger 1 (0.1 .. 1.0)[div]
	float HoldoffTr1;         // Offset 98h : Holdoff trigger 1 [s]
	float LevelTr1;           // Offset 9Ch : Level trigger 1 [V]
	uint8_t EdgeSlopeTr1;     // Offset A0h : Edge slope trigger 1 (0=Rising, 1=Falling, 2=Both)
	uint8_t PulseCondTr1;     // Offset A1h : Pulse condition trigger 1 (0=Pos>, 1=Pos<, 2=Pos=, 3=Neg>, 4=Neg<, 5=Neg=)
	uint8_t wfm_A2h[2];       // Offset A2h
	float PulseWidthTr1;      // Offset A4h : Pulse width trigger 1 [s]
	uint8_t SlopeCondTr1;     // Offset A8h : Slope condition trigger 1 (0=Rise>, 1=Rise<, 2=Rise=, 3=Fall>, 4=Fall<, 5=Fall=)
	uint8_t wfm_A9h[3];       // Offset A9h
	float Level2Tr1;          // Offset ACh : Level 2 trigger 1 [V]
	float SlopeTimeTr1;       // Offset B0h : Slope time trigger 1 [s]
	uint8_t VideoPolTr1;      // Offset B4h : Video polarity trigger 1 (0=Pos, 1=Neg)
	uint8_t VideoSyncTr1;     // Offset B5h : Video sync. trigger 1 (0=All Lines, 1=Line Num, 2=Odd Field, 3=Even Field)
	uint8_t VideoStdTr1;      // Offset B6h : Video standard trigger 1 (0=NTSC, 1=PAL/SECAM)
	// Trigger 2 parameters
	uint8_t ModeTr2;          // Offset B7h : Mode trigger 2 (0=Edge, 1=Pulse, 2=Slope, 3=Video)
	uint8_t SourceTr2;        // Offset B8h : Source trigger 2 (0=CH1, 1=CH2, 2=EXT, 3=AC Line)
	uint8_t CouplingTr2;      // Offset B9h : Coupling trigger 2 (0=DC, 1=LF Reject, 2=HF Reject, 3=AC)
	uint8_t SweepTr2;         // Offset BAh : Sweep trigger 2 (0=Auto, 1=Normal, 2=Single)
	uint8_t wfm_BBh[1];       // Offset BBh
	float SensitivityTr2;     // Offset BCh : Sensitivity trigger 2 (0.1 .. 1.0) [div]
	float HoldoffTr2;         // Offset C0h : Holdoff trigger 2 [s]
	float LevelTr2;           // Offset C4h : Level trigger 2 level [V]
	uint8_t EdgeSlopeTr2;     // Offset C8h : Edge slope trigger 2 (0=Rising, 1=Falling, 2=Both)
	uint8_t PulseCondTr2;     // Offset C9h : Pulse condition trigger 2 (0=Pos>, 1=Pos<, 2=Pos=, 3=Neg>, 4=Neg<, 5=Neg=)
	uint8_t wfm_CAh[2];       // Offset CAh
	float PulseWidthTr2;      // Offset CCh : Pulse width trigger 2 [s]
	uint8_t SlopeCondTr2;     // Offset D0h : Slope condition trigger 2 (0=Pos>, 1=Pos<, 2=Pos=, 3=Neg>, 4=Neg<, 5=Neg=)
	uint8_t wfm_D1h[3];       // Offset D1h
	float Level2Tr2;          // Offset D4h : Level 2 trigger 2 [V]
	float SlopeTimeTr2;       // Offset D8h : Slope time trigger 2 [s]
	uint8_t VideoPolTr2;      // Offset DCh : Video polarity trigger 2 (0=Pos, 1=Neg)
	uint8_t VideoSyncTr2;     // Offset DDh : Video sync trigger 2 (0=All Lines, 1=Line Num, 2=Odd Field, 3=Even Field)
	uint8_t VideoStdTr2;      // Offset DEh : Video standard trigger 2 (0=NTSC, 1=PAL/SECAM)
	uint8_t wfm_DFh[9];       // Offset DFh
	// Additional alternate trigger parameters
	uint32_t PtsCh2ATr;       // Offset E8h : Data points channel 2 alternate trigger mode (8k, 512k)
	// Time base 2 parameters
	uint64_t hScaleDT2;       // Offset ECh : Horizontal scale display time 2 [ps/div]
	int64_t hPosDT2;          // Offset F4h : Horizontal position display time 2 [ps/div]
	float SampleRateT2;       // Offset FCh : Sample rate time base 2 [Hz]
	uint64_t hScaleMT2;       // Offset 100h : Horizontal scale memory time 2 [ps/div]
	int64_t hPosMT2;          // Offset 108h : Horizontal position memory time 2 [ps/div]
	/* Additional logic analyzer parameters */
	float SampleRateLA;       // Offset 110h : Sample rate logic analyzer [Hz]
	// beginning of data      // Offset 114h : CH1, CH2, LA (1 byte per sample)
} RigolDS1000WFM_t;


/* Structure of RIFF-WAVE file header
 */
typedef struct {
	// RIFF-WAVE chunk
	char     ID[4];          // Offset 00h : RIFF ID = 'R' 'I' 'F' 'F' = 0x52494646
	uint32_t Size;           // Offset 04h : RIFF chunk size = file size [byte] - 8 byte
	char     Type[4];        // Offset 08h : RIFF type = 'W' 'A' 'V' 'E'" = 57415645h
	// Format subchunk
	char     FmtID[4];       // Offset 0Ch : Subchunk ID = 'f' 'm' 't' ' ' = 666d7420h
	uint32_t FmtSize;        // Offset 10h : Subchunk size = 16 byte (const)
	uint16_t FmtTag;         // Offset 14h : Format tag: 0x0001 = PCM (linear quantization)
	uint16_t Channels;       // Offset 16h : Number of channels (1=mono, 2=stereo)
	uint32_t SampleRate;     // Offset 18h : Samples per second [Hz]
	uint32_t BytesPerSec;    // Offset 1Ah : Average bytes per second (SampleRate * FrameSize)
	uint16_t FrameSize;      // Offset 20h : Frame size = channels * ((BitsPerSample + 7) / 8)
	uint16_t BitsPerSample;  // Offset 22h : Bits per sample = 8 bit (Rigol DS1052e)
	// Data subchunk
	char     DataID[4];      // Offset 24h : Subchunk ID = 'd' 'a' 't' 'a' = 64617461h
	uint32_t DataSize;       // Offset 28h : Subchunk size = number of data bytes
//	frame_t  Frames[]        // Offset 2Ch : Begin of data
} RiffWave_t;

#endif /* WFMEXTRACTOR_H_ */
