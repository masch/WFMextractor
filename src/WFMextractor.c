/**
 * @file
 * @mainpage
 *
 * @code
 * Program name   : WFMextractor
 *
 * Description    : Tool to extract the measurement data and configuration
 *                  settings out of a Rigol DS1000E wave format file (*.wfm).
 *
 *                  The measurement data can be stored in CSV and/or RIFF-WAVE
 *                  file format for further processing.
 *
 *                  Along with the CSV file, a gnuplot script file can be generated
 *                  to be passed to gnuplot to draw a graph which looks similarly
 *                  to the oscilloscope display output.
 *
 *                  Finally a text file can be generated that lists the oscilloscope
 *                  settings stored additionally in the measurement file.
 *
 * Invocation     : WFMextractor -c -g -t -v -w *.wfm
 *                  -c     Write measurement data into CSV file.
 *                  -g     Generate gnuplot script file.
 *                  -t     Output oscilloscope settings into text file.
 *                  -v     Print program version to stdout.
 *                  -w     Write measurement data into RIFF-WAVE file.
 *                  *.wfm  Rigol DS1000E oscilloscope waveform file(s).
 *
 * Author         : Marcel Schlottmann
 *
 * License        : GPL
 *
 * Last change    : 2016-08-16
 * @endcode
 * @n
 *
 * @dot
 * digraph "Data flow" {
 *    node [shape="ellipse"];
 *    n01  [label="start", style="bold", URL="\ref main"];
 *    node [shape="box"];
 *    n02  [label="check_invocation()", URL="\ref check_invocation"];
 *    n03  [label="check_wfm_file()",   URL="\ref check_wfm_file"];
 *    n04  [label="write_csv_file()",   URL="\ref write_csv_file"];
 *    n05  [label="write_gp_file()",    URL="\ref write_gp_file"];
 *    n06  [label="write_txt_file()",   URL="\ref write_txt_file"];
 *    n07  [label="write_wav_file()",   URL="\ref write_wav_file"];
 *    n01 -> n02;
 *    n02 -> n03;
 *    n03 -> {n04, n05, n06, n07} [style="dashed"];
 * }
 * @enddot
 * @n
 */

#include "WFMextractor.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>


/* ************************************************************************** *
 *                           Macro definition(s)
 * ************************************************************************** */

/**
 * Program version (date of last change)
 */
#define PROGVERSION "2016-08-16"

/**
 * Maximum allowed file name length (including path)
 */
#define MAXSTRLEN 256


/* ************************************************************************** *
 *                        Module (global) variables
 * ************************************************************************** */

/**
 * Name of *.wfm file to open (including path)
 */
char wfm_file_name[MAXSTRLEN];

/**
 * File handler of *.wfm file opened
 */
FILE *wfm_file;

/**
 * File header structure that contains oscilloscope settings stored at the
 * beginning of the *.wfm file
 */
RigolDS1000WFM_t wfm_header;

/**
 * Flag that signs program version to be printed to stdout
 */
bool print_ver = 0;

/**
 * Flag that signs a CSV file to be written
 */
bool write_csv = 0;

/**
 * Flag that signs a GnuPlot script file to be written
 */
bool write_gp = 0;

/**
 * Flag that signs a TXT info file to be written
 */
bool write_txt = 0;

/**
 * Flag that signs a RIFF-WAVE file to be written
 */
bool write_wav = 0;


/* ************************************************************************** *
 *                            Functions definition
 * ************************************************************************** */

/**
 * Function to check the program invocation.
 *
 * The program invocation is checked regarding program options (-c -g, -cgtvw, ..)
 * and the *.wfm file name(s) ending.
 * In case of an invalid program option or file name ending an error is detected
 * and a short program invocation instruction is printed to stdout.
 *
 * @param[in]  argc  argument counter
 * @param[in]  argv  argument vector
 *
 * @retval  2..n  Argument vector index of the first *.wfm file to be processed.
 * @retval  0     Only program version is requested (-v option given).
 * @retval -1     Error while program invocation.
 *
 * <b>Global variables</b>@n
 * @link print_ver print_ver @endlink <c>[in, out]</c>,
 * @link write_csv write_csv @endlink <c>[in, out]</c>,
 * @link write_gp  write_gp  @endlink <c>[in, out]</c>,
 * @link write_txt write_txt @endlink <c>[in, out]</c>,
 * @link write_wav write_wav @endlink <c>[in, out]</c>,
 */
int check_invocation(int argc, char *argv[])
{
	char file_name[MAXSTRLEN];
	char *str;
	int i,j;

	/* Check number of invocation arguments: Expected minimum = 2 ("prog -v")
	 */
	if (argc < 2) goto error;

	/* Check, if first parameter starts with a dash as sign of option parameter
	 * ("-c", "-cgtvw", "-...")
	 */
	if (argv[1][0] != '-') goto error;

	/* Parse program options: "-c", "-c -g", "-cgtvw", etc. */
	for (i = 1; (i < argc) && (argv[i][0] == '-'); i++) {
		if ((argv[i][0] == '-') && (argv[i][1] == '\0')) goto error;
		for (j = 1; argv[i][j] != '\0'; j++) {
			switch (argv[i][j]) {
			case 'c':
				write_csv = true;
				break;
			case 'g':
				write_gp = true;
				break;
			case 't':
				write_txt = true;
				break;
			case 'v':
				print_ver = true;
				break;
			case 'w':
				write_wav = true;
				break;
			default:
				goto error;
				break;
			}
		}
	}

	/* If requested, print program version to stdout. */
	if (print_ver) printf ("WFMextractor version %s\n", PROGVERSION);

	/* In case an output file has to be written, ensure that at least one
	 * measurement file is given after the program option(s). */
	if (write_csv || write_gp || write_txt || write_wav)
	{
		if (argc <= i) goto error;

		/* Check file name(s) ending (*.wfm, *.WFM) */
		for (j = i; j < argc; j++) {
			strncpy(file_name, argv[j], (MAXSTRLEN-1));
			str = strrchr(file_name, '.');
			if (str == NULL) goto error;
			if ((str[1] != 'w') && (str[1] != 'W')) goto error;
			if ((str[2] != 'f') && (str[2] != 'F')) goto error;
			if ((str[3] != 'm') && (str[3] != 'M')) goto error;
			if (str[4] != '\0') goto error;
		}
		/* Return argument vector index of first *.wfm file given. */
		return i;
	} else {
		return 0;
	}

error:
	printf("\n");
	printf("WFMextractor -c -g -t -v -w *.wfm\n");
	printf("---------------------------------\n");
	printf("-c     Write measurement data into CSV file.\n");
	printf("-g     Generate gnuplot script file.\n");
	printf("-t     Output oscilloscope settings into text file.\n");
	printf("-v     Print program version to stdout.\n");
	printf("-w     Write measurement data into RIFF-WAVE file.\n");
	printf("*.wfm  Rigol DS1000E oscilloscope waveform file(s).\n");
	printf("\n");

	return -1;
}


/**
 * Function that does some plausibility checks on the *.wfm file.
 *
 * The *.wfm file / file header is checked regarding:
 * - File ID, which shall be 0xA5A5
 * - Probe attenuation, which shall be either 1X or 10X
 * - Logic analyzer data, which shall not be included in the file
 * - Amount of data points, which are expected to be stored in the file
 *
 * @retval  0  No error.
 * @retval -1  Error while plausibility check.
 *
 * <b>Global variables</b>@n
 * @link wfm_file_name wfm_file_name @endlink <c>[in]</c>,
 * @link wfm_file      wfm_file      @endlink <c>[in]</c>,
 * @link wfm_header    wfm_header    @endlink <c>[in]</c>
 */
int check_wfm_file(void)
{
	uint32_t i, j;

	/* Check file ID */
	if (wfm_header.FileID != 0xA5A5) {
		printf("Unknown file ID (%Xh) in %s!\n", wfm_header.FileID, wfm_file_name);
		goto error;
	}

	/* Check probe attenuation */
	if (((wfm_header.ProbeCh1 != 0x3F80) && (wfm_header.ProbeCh1 != 0x4120)) ||
		((wfm_header.ProbeCh2 != 0x3F80) && (wfm_header.ProbeCh2 != 0x4120))) {
		printf("Sorry, only 1X and 10X probes are supported yet.\n");
		goto error;
	}

	/* Check if logic analyzer data are included */
	if ((wfm_header.LA1data != 0) || (wfm_header.LA2data != 0)) {
		printf("Sorry, file contains logic analyzer data which are not supported.\n");
		goto error;
	}

	/* Check amount of data */

	/* Set input file pointer to the beginning of the data field */
	if (fseek(wfm_file, sizeof(wfm_header), SEEK_SET) == -1) {
		printf("Error while reading file %s!\n", wfm_file_name);
		goto error;
	}

	/* Count data points in file */
	for (i = 0; fgetc(wfm_file) != EOF; i++);

	if (i == 0) {
		printf("%s contains no data!\n", wfm_file_name);
		goto error;
	}

	/* Calculate expected data points */
	j  = (wfm_header.CH1data == 1) ? wfm_header.PtsPerCh : 0;
	j += (wfm_header.CH2data == 1) ? wfm_header.PtsPerCh : 0;

	/* Compare counted and expected data points */
	if (i < j) {
		printf("%s contains less data than expected (%d < %d)!\n", wfm_file_name, i, j);
		goto error;
	}

	if (i > j) {
		printf("%s contains more data than expected (%d > %d)!\n", wfm_file_name, i, j);
		goto error;
	}

	return 0;
error:
	return -1;
}


/**
 * Function that outputs *.wfm file measurement data into a *.cvs file.
 *
 * The Function reads out and converts the measurement data of the *.wfm file
 * and writes it into a *.cvs file. X-values are stored in second [s], Y-values
 * in Volt [V]. The trigger point is always located at zero second and all other
 * measurement points are properly aligned around with respect to the horizontal
 * scale and position setting of the oscilloscope.
 *
 * @retval  0  No error.
 * @retval -1  Error while opening *.cvs file or memory allocation.
 *
 * <b>Global variables</b>@n
 * @link wfm_file_name wfm_file_name @endlink <c>[in]</c>,
 * @link wfm_file      wfm_file      @endlink <c>[in]</c>,
 * @link wfm_header    wfm_header    @endlink <c>[in]</c>
 */
int write_csv_file(void)
{
	char file_name[MAXSTRLEN];
	FILE *file;

	struct {double x1; double x2; ; float ch1; float ch2;} *data;
	uint32_t i, numpts;

	uint32_t vScaleCh1, vScaleCh2;

	/* Determine conversion factor for CH1 */
	if (wfm_header.ProbeCh1 == 0x3F80) vScaleCh1 = wfm_header.vScaleCh1M;
	else vScaleCh1 = wfm_header.vScaleCh1M * 10;

	/* Determine conversion factor for CH2 */
	if (wfm_header.ProbeCh2 == 0x3F80) vScaleCh2 = wfm_header.vScaleCh2M;
	else vScaleCh2 = wfm_header.vScaleCh2M * 10;

	/* Determine number of data points */
	if (wfm_header.RollStpPt > 0) numpts = wfm_header.RollStpPt + 1;
	else numpts = wfm_header.PtsPerCh;

	/* Allocate memory */
	data = calloc(sizeof(*data), numpts);
	if (data == NULL) {
		printf("Memory allocation failed!\n");
		goto error;
	}

	/* -----------------------
	 * Calculation of x-values
	 * -----------------------
	 * For Roll Mode (stop point always at 0s, all other x-values negative,):
	 *    x-start    = -1 * (number of data points -1) / sample rate
	 *    x-stop     = 0s
	 *    resolution = 1 / sample rate
	 * For Trigger Mode (trigger point always at 0s, x-values aligned around):
	 *    x-start    = horizontal position - (number of data points / 2) / sample rate
	 *    x-stop     = horizontal psition + ((number of data points / 2) -1) / sample rate
	 *    resolution = 1 / sample rate
	 */

	/* Store timestamp of first data point into memory*/
	if (wfm_header.RollStpPt > 0) {
		data[0].x1 = -1.0 * (numpts - 1) / (double)wfm_header.SampleRateT1;
	} else {
		data[0].x1 = -0.5 * numpts / (double)wfm_header.SampleRateT1;
		data[0].x1 += wfm_header.hPosMT1 / 1e12;
	}

	/* Store timestamps for all other data points into memory */
	for (i = 1; i < numpts; i++)
		data[i].x1 = data[0].x1 + i / (double)wfm_header.SampleRateT1;

	/* For alternate trigger mode repeat timestamp calculation for second x-axes */
	if (wfm_header.TriggerMode == 4) {
		data[0].x2 = -0.5 * numpts / (double)wfm_header.SampleRateT2;
		data[0].x2 += wfm_header.hPosMT2 / 1e12;
		for (i = 1; i < numpts; i++)
			data[i].x2 = data[0].x2 + i / (double)wfm_header.SampleRateT2;
	}

	/* -----------------------------
	 * Calculation of Ch1/Ch2 values
	 * -----------------------------
	 * 1. If wfm data is stored non inverted:
	 *               (screen offset - wfm data - vertical position) [pixel]   vertical scale [µV/div]
	 *    csv data = ------------------------------------------------------ * -----------------------
	 *                                   25 [pixel/div]                             1e6 [µV/V]
	 *    If wfm data is stored inverted:
	 *               (wfm data - vertical position - screen offset) [pixel]   vertical scale [µV/div]
	 *    csv data = ------------------------------------------------------ * -----------------------
	 *                                   25 [pixel/div]                             1e6 [µV/V]
	 * 2. If the polarity is changed after measurement (display data inverted != memory data inverted)
	 *    the sign of the csv data is changed once more.
	 */

	/* If channel 1 data are available, read in and prepare CH1 data into memory */
	if (wfm_header.CH1data == 1) {
		/* Set *.wfm file pointer to the beginning of CH1 data */
		fseek(wfm_file, sizeof(wfm_header), SEEK_SET);

		/* Prepare CH1 data with respect of inverted storage and scaling */
		if (wfm_header.MemCh1Inv == 0) {
			for (i = 0; i < numpts; i++)
				data[i].ch1 = (125.0 - fgetc(wfm_file) - wfm_header.vPosCh1M) * vScaleCh1 / (25*1e6);
		} else {
			for (i = 0; i < numpts; i++)
				data[i].ch1 = (fgetc(wfm_file) - wfm_header.vPosCh1M - 125.0) * vScaleCh1 / (25*1e6);
		}
		if (wfm_header.DispCh1Inv != wfm_header.MemCh1Inv)
			for (i = 0; i < numpts; i++)
				data[i].ch1 = -data[i].ch1;
	}

	/* If channel 2 data are available, read in and prepare CH2 data into memory */
	if (wfm_header.CH2data == 1) {
		/* Set *.wfm file pointer to the beginning of CH2 data */
		if (wfm_header.CH1data == 1) {
			fseek(wfm_file, (sizeof(wfm_header) + wfm_header.PtsPerCh), SEEK_SET);
		} else {
			fseek(wfm_file, sizeof(wfm_header), SEEK_SET);
		}

		/* Prepare CH2 data with respect of inverted storage and scaling */
		if (wfm_header.MemCh2Inv == 0) {
			for (i = 0; i < numpts; i++)
				data[i].ch2 = (125.0 - fgetc(wfm_file) - wfm_header.vPosCh2M) * vScaleCh2 / (25*1e6);
		} else {
			for (i = 0; i < numpts; i++)
				data[i].ch2 = (fgetc(wfm_file) - wfm_header.vPosCh2M - 125.0) * vScaleCh2 / (25*1e6);
		}
		if (wfm_header.DispCh2Inv != wfm_header.MemCh2Inv)
			for (i = 0; i < numpts; i++)
				data[i].ch2 = -data[i].ch2;
	}

	/* Open output file */
	strcpy(file_name, wfm_file_name);
	strcpy(strstr(file_name, ".wfm"), ".csv" );

	file = fopen(file_name, "w");
	if (file == NULL) {
		printf("Could not open %s!\n", file_name);
		goto error;
	}

	/* Write *.csv data */
	if ((wfm_header.CH1data == 1) && (wfm_header.CH2data == 1)) {
		/* Write data of CH1 and CH2 */
		if (wfm_header.TriggerMode == 4) {
			fprintf(file, "X(CH1),CH1,X(CH2),CH2\r\n[s],[V],[s],[V]\r\n");
			for (i = 0; i < numpts; i++)
				fprintf(file, "% .6e,% .4e,% .6e,% .4e\r\n", data[i].x1, data[i].ch1, data[i].x2, data[i].ch2);
		} else {
			fprintf(file, "X,CH1,CH2\r\n[s],[V],[V]\r\n");
			for (i = 0; i < numpts; i++)
				fprintf(file, "% .6e,% .4e,% .4e\r\n", data[i].x1, data[i].ch1, data[i].ch2);
		}
	} else {
		/* Write data of CH1 or CH2 */
		if (wfm_header.CH1data == 1){
			fprintf(file, "X,CH1\r\n[s],[V]\r\n");
			for (i = 0; i < numpts; i++)
				fprintf(file, "% .6e,% .4e\r\n", data[i].x1, data[i].ch1);
		} else {
			fprintf(file, "X,CH2\r\n[s],[V]\r\n");
			if (wfm_header.TriggerMode == 4) {
				for (i = 0; i < numpts; i++)
					fprintf(file, "% .6e,% .4e\r\n", data[i].x2, data[i].ch2);
			} else {
				for (i = 0; i < numpts; i++)
					fprintf(file, "% .6e,% .4e\r\n", data[i].x1, data[i].ch2);
			}
		}
	}

	fclose(file);
	free(data);
	return 0;
error:
	free(data);
	return -1;
}


/**
 * Function that outputs a gnuplot script file which can be used to generate a
 * graph similarly to the oscilloscope screen.
 *
 * The Function reads out measurement settings of the *.wfm file header and
 * outputs a *.gp script file which can be passed to gnuplot to generate a graph
 * that looks similarly to the curves adjusted on the oscilloscope screen.
 * The script file refers to the corresponding *.csv file, so it has to be
 * generated in addition to the *.gp file to get a graph plotted.
 *
 * @retval  0  No error.
 * @retval -1  Error while opening *.gp file.
 *
 * <b>Global variables</b>@n
 * @link wfm_file_name wfm_file_name @endlink <c>[in]</c>,
 * @link wfm_header    wfm_header    @endlink <c>[in]</c>
 */
int write_gp_file(void)
{
	char file_name[MAXSTRLEN];
	FILE *file;

	enum {sec = 1, millisec = 1000, microsec = 1000000, nanosec = 1000000000} X1con, X2con;

	enum {volt = 1, millivolt = 1000} Ch1con, Ch2con;
	uint32_t vScaleCh1, vScaleCh2;
	int16_t vPosCh1, vPosCh2;

	struct {double min; double max; double incr;} XaxleCh1, XaxleCh2;
	struct {float min; float max; float incr;} YaxleCh1, YaxleCh2;

	struct {double x1; float y1; double x2; float y2;}
		TrTimeInstCh1, TrTimeInstCh2, TrLevel1Ch1, TrLevel2Ch1, TrLevel1Ch2, TrLevel2Ch2;

	/* Determine conversion factor for time base 1 */
	if (wfm_header.hScaleDT1 >= 1e12) X1con = sec;
	else if	(wfm_header.hScaleDT1 >= 1e9) X1con = millisec;
	else if	(wfm_header.hScaleDT1 >= 1e6) X1con = microsec;
	else X1con = nanosec;

	/* Determine conversion factor for time base 2 */
	if (wfm_header.hScaleDT2 >= 1e12) X2con = sec;
	else if	(wfm_header.hScaleDT2 >= 1e9) X2con = millisec;
	else if	(wfm_header.hScaleDT2 >= 1e6) X2con = microsec;
	else X2con = nanosec;

	/* Determine conversion factor for CH1 */
	if (wfm_header.ProbeCh1 == 0x3F80) vScaleCh1 = wfm_header.vScaleCh1D;
	else vScaleCh1 = wfm_header.vScaleCh1D * 10;
	if (vScaleCh1 >= 1e6) Ch1con = volt;
	else Ch1con = millivolt;

	/* Determine conversion factor for CH2 */
	if (wfm_header.ProbeCh2 == 0x3F80) vScaleCh2 = wfm_header.vScaleCh2D;
	else vScaleCh2 = wfm_header.vScaleCh2D * 10;
	if (vScaleCh2 >= 1e6) Ch2con = volt;
	else Ch2con = millivolt;

	/* Determine vertical offset for CH1 and CH2 (unit = div) */
	if (wfm_header.vPosCh1D >= 0) vPosCh1 = (wfm_header.vPosCh1D + 18) / 25;
	else  vPosCh1 = (wfm_header.vPosCh1D - 18) / 25;

	if (wfm_header.vPosCh2D >= 0) vPosCh2 = (wfm_header.vPosCh2D + 18) / 25;
	else  vPosCh2 = (wfm_header.vPosCh2D - 18) / 25;

	/* Calculate horizontal axle scaling */
	XaxleCh1.incr = (double)wfm_header.hScaleDT1 * X1con / 1e12;
	XaxleCh1.min = XaxleCh1.max = (double)wfm_header.hPosDT1 * X1con / 1e12;
	if (wfm_header.RollStpPt > 0) {
		XaxleCh1.min -= 12 * XaxleCh1.incr;
		XaxleCh1.max +=  0 * XaxleCh1.incr;
	} else {
		XaxleCh1.min -= 6 * XaxleCh1.incr;
		XaxleCh1.max += 6 * XaxleCh1.incr;
	}

	/* For alternate trigger mode repeat calculation for second x-axes */
	if(wfm_header.TriggerMode == 4) {
		XaxleCh2.incr = (double)wfm_header.hScaleDT2 * X2con / 1e12;
		XaxleCh2.min = XaxleCh2.max = (double)wfm_header.hPosDT2 * X2con / 1e12;
		XaxleCh2.min -= 6 * XaxleCh2.incr;
		XaxleCh2.max += 6 * XaxleCh2.incr;
	} else {
		XaxleCh2.incr = XaxleCh1.incr;
		XaxleCh2.min = XaxleCh1.min;
		XaxleCh2.max = XaxleCh1.max;
	}

	/* Calculate vertical axle scaling */
	YaxleCh1.incr = (float)vScaleCh1 * Ch1con / 1e6;
	YaxleCh1.min = (-5 - vPosCh1) * YaxleCh1.incr;
	YaxleCh1.max =  (5 - vPosCh1) * YaxleCh1.incr;

	YaxleCh2.incr = (float)vScaleCh2 * Ch2con / 1e6;
	YaxleCh2.min = (-5 - vPosCh2) * YaxleCh2.incr;
	YaxleCh2.max =  (5 - vPosCh2) * YaxleCh2.incr;

	/* Calculate trigger time instant coordinates */
	TrTimeInstCh1.x1 = TrTimeInstCh1.x2 = 0.0;
	TrTimeInstCh1.y1 = YaxleCh1.min;
	TrTimeInstCh1.y2 = YaxleCh1.max;

	TrTimeInstCh2.x1 = TrTimeInstCh2.x2 = 0.0;
	TrTimeInstCh2.y1 = YaxleCh2.min;
	TrTimeInstCh2.y2 = YaxleCh2.max;

	/* Calculate trigger level coordinates */
	TrLevel1Ch1.x1 = TrLevel2Ch1.x1 = XaxleCh1.min;
	TrLevel1Ch1.x2 = TrLevel2Ch1.x2 = XaxleCh1.max;
	if (wfm_header.DispCh1Inv == wfm_header.MemCh1Inv) {
		TrLevel1Ch1.y1 = TrLevel1Ch1.y2 = wfm_header.LevelTr1 * Ch1con;
		TrLevel2Ch1.y1 = TrLevel2Ch1.y2 = wfm_header.Level2Tr1 * Ch1con;
	} else {
		TrLevel1Ch1.y1 = TrLevel1Ch1.y2 = -1.0 * wfm_header.LevelTr1 * Ch1con;
		TrLevel2Ch1.y1 = TrLevel2Ch1.y2 = -1.0 * wfm_header.Level2Tr1 * Ch1con;
	}

	TrLevel1Ch2.x1 = TrLevel2Ch2.x1 = XaxleCh2.min;
	TrLevel1Ch2.x2 = TrLevel2Ch2.x2 = XaxleCh2.max;
	if (wfm_header.DispCh2Inv == wfm_header.MemCh2Inv) {
		/* In case of alternate trigger mode, trigger 2 settings shall be used for CH2 */
		if (wfm_header.TriggerMode == 4) {
			TrLevel1Ch2.y1 = TrLevel1Ch2.y2 = wfm_header.LevelTr2 * Ch2con;
			TrLevel2Ch2.y1 = TrLevel2Ch2.y2 = wfm_header.Level2Tr2 * Ch2con;
		} else {
			TrLevel1Ch2.y1 = TrLevel1Ch2.y2 = wfm_header.LevelTr1 * Ch2con;
			TrLevel2Ch2.y1 = TrLevel2Ch2.y2 = wfm_header.Level2Tr1 * Ch2con;
		}
	} else {
		if (wfm_header.TriggerMode == 4) {
			TrLevel1Ch2.y1 = TrLevel1Ch2.y2 = -1.0 * wfm_header.LevelTr2 * Ch2con;
			TrLevel2Ch2.y1 = TrLevel2Ch2.y2 = -1.0 * wfm_header.Level2Tr2 * Ch2con;
		} else {
			TrLevel1Ch2.y1 = TrLevel1Ch2.y2 = -1.0 * wfm_header.LevelTr1 * Ch2con;
			TrLevel2Ch2.y1 = TrLevel2Ch2.y2 = -1.0 * wfm_header.Level2Tr1 * Ch2con;
		}
	}

	/* Open output file */
	strcpy(file_name, wfm_file_name);
	strcpy(strstr(file_name, ".wfm"), ".gp" );

	file = fopen(file_name, "w");
	if (file == NULL) {
		printf("Could not open %s!\n", file_name);
		goto error;
	}

	/* Prepare file name without *.gp extension for further processing */
	strcpy(strstr(file_name, ".gp"), "" );

	/* Write gnuplot script file */
	fprintf(file, "# ###################\n");
	fprintf(file, "# Gnuplot script file\n");
	fprintf(file, "# ###################\n");
	fprintf(file, "#\n");

	fprintf(file, "# Example settings for *.eps diagram\n");
	fprintf(file, "# set title \"%s.eps\" font \"Sans,20\"\n", strrchr(file_name, '/') ? (strrchr(file_name, '/') + 1) : file_name);
	fprintf(file, "# set terminal epscairo font \"Sans,16\" fontscale 0.5 linewidth 3 size 10inch,8inch\n");
	fprintf(file, "# set output \"%s.eps\"\n", file_name);
	fprintf(file, "# test\n");
	fprintf(file, "#\n");

	fprintf(file, "# Example settings for *.pdf diagram\n");
	fprintf(file, "# set title \"%s.pdf\" font \"Sans,20\"\n",  strrchr(file_name, '/') ? (strrchr(file_name, '/') + 1) : file_name);
	fprintf(file, "# set terminal pdfcairo font \"Sans,16\" fontscale 0.5 linewidth 3 size 10inch,8inch\n");
	fprintf(file, "# set output \"%s.pdf\"\n", file_name);
	fprintf(file, "# test\n");
	fprintf(file, "#\n");

	fprintf(file, "# Example settings for *.png diagram\n");
	fprintf(file, "# set title \"%s.png\" font \"Sans,20\"\n", strrchr(file_name, '/') ? (strrchr(file_name, '/') + 1) : file_name);
	fprintf(file, "# set terminal pngcairo font \"Sans,16\" fontscale 1.0 size 1280,1024\n");
	fprintf(file, "# set output \"%s.png\"\n", file_name);
	fprintf(file, "# test\n");
	fprintf(file, "#\n");

	fprintf(file, "# Example settings for *.svg diagram\n");
	fprintf(file, "set title \"%s.svg\" font \"Sans,20\"\n",  strrchr(file_name, '/') ? (strrchr(file_name, '/') + 1) : file_name);
	fprintf(file, "set terminal svg fname \"Sans\" fsize 16 background \"white\" size 1280,1024 dynamic\n");
	fprintf(file, "set output \"%s.svg\"\n", file_name);
	fprintf(file, "# test\n");
	fprintf(file, "#\n");

	fprintf(file, "# Enable grid lines\n");
	fprintf(file, "set grid back\n");
	fprintf(file, "#\n");

	fprintf(file, "# Set data separator to ',' because of *.csv data file\n");
	fprintf(file, "set datafile separator \",\"\n");
	fprintf(file, "#\n");

	/* Gnuplot's multiplot mode is used if two channels are stored in alternate trigger mode. */
	if ((wfm_header.TriggerMode == 4) && (wfm_header.CH1data == 1) && (wfm_header.CH2data == 1)) {
		fprintf(file, "# Enable multiplot mode (2 rows, 1 column)\n");
		fprintf(file, "set multiplot layout 2,1\n");
		fprintf(file, "#\n");

		/* 1st plot (CH1) */
		fprintf(file, "# Horizontal scaling CH1\n");
		fprintf(file, "set xlabel \"Time %s\" font \"Sans,16\"\n",
				(X1con == sec) ? "[s]" : (X1con == millisec) ? "[ms]" : (X1con == microsec) ? "[µs]" : "[ns]");
		fprintf(file, "set xrange [%g:%g]\n", XaxleCh1.min, XaxleCh1.max);
		fprintf(file, "set xtics out %g font \"Sans,14\" nomirror\n", XaxleCh1.incr);
		fprintf(file, "#\n");

		fprintf(file, "# Vertical scaling CH1\n");
		fprintf(file, "set ylabel \"Channel 1 %s\" font \"Sans,16\"\n", (Ch1con == volt) ? "[V]" : "[mV]");
		fprintf(file, "set yrange [%g:%g]\n", YaxleCh1.min, YaxleCh1.max);
		fprintf(file, "set ytics out %g font \"Sans,14\" nomirror\n", YaxleCh1.incr);
		fprintf(file, "#\n");

		fprintf(file, "# Trigger time instant and level CH1\n");
		fprintf(file, "set arrow 1 from %g,%g to %g,%g size character 1.0,150 filled front\n",
				TrTimeInstCh1.x1, TrTimeInstCh1.y1, TrTimeInstCh1.x2, TrTimeInstCh1.y2);
		fprintf(file, "set arrow 2 from %g,%g to %g,%g nohead front linetype 0 linecolor 1\n",
				TrLevel1Ch1.x1, TrLevel1Ch1.y1, TrLevel1Ch1.x2, TrLevel1Ch1.y2);
		/* In case of slope trigger mode add second trigger level line. */
		if (wfm_header.ModeTr1 == 2) {
			fprintf (file, "set arrow 3 from %g,%g to %g,%g nohead front linetype 0 linecolor 1\n",
					TrLevel2Ch1.x1, TrLevel2Ch1.y1, TrLevel2Ch1.x2, TrLevel2Ch1.y2);
		}
		fprintf(file, "#\n");

		fprintf(file, "# Plot diagram CH1\n");
		fprintf(file, "plot \"%s.csv\" skip 2 using ($1*%u):($2*%u) with lines linecolor 1 notitle\n",
				file_name, X1con, Ch1con);
		fprintf(file, "#\n");

		/* 2nd plot (CH2) */
		fprintf(file, "# Horizontal scaling CH2\n");
		fprintf(file, "set xlabel \"Time %s\" font \"Sans,16\"\n",
				(X2con == sec) ? "[s]" : (X2con == millisec) ? "[ms]" : (X2con == microsec) ? "[µs]" : "[ns]");
		fprintf(file, "set xrange [%g:%g]\n", XaxleCh2.min, XaxleCh2.max);
		fprintf(file, "set xtics out %g font \"Sans,14\" nomirror\n", XaxleCh2.incr);
		fprintf(file, "#\n");

		fprintf(file, "# Vertical scaling CH2\n");
		fprintf(file, "set ylabel \"Channel 2 %s\" font \"Sans,16\"\n", (Ch2con == volt) ? "[V]" : "[mV]");
		fprintf(file, "set yrange [%g:%g]\n", YaxleCh2.min, YaxleCh2.max);
		fprintf(file, "set ytics out %g font \"Sans,14\" nomirror\n", YaxleCh2.incr);
		fprintf(file, "#\n");

		fprintf(file, "# Trigger time instant and level CH2\n");
		fprintf(file, "set arrow 1 from %g,%g to %g,%g size character 1.0,150 filled front\n",
				TrTimeInstCh2.x1, TrTimeInstCh2.y1, TrTimeInstCh2.x2, TrTimeInstCh2.y2);
		fprintf(file, "set arrow 2 from %g,%g to %g,%g nohead front linetype 0 linecolor 2\n",
				TrLevel1Ch2.x1, TrLevel1Ch2.y1, TrLevel1Ch2.x2, TrLevel1Ch2.y2);
		/* In case of slope trigger mode add second trigger level line. */
		if (wfm_header.ModeTr2 == 2) {
			fprintf(file, "set arrow 3 from %g,%g to %g,%g nohead front linetype 0 linecolor 2\n",
					TrLevel2Ch2.x1, TrLevel2Ch2.y1, TrLevel2Ch2.x2, TrLevel2Ch2.y2);
		} else {
			/* Second trigger level line of CH1 has to be deleted if it is not reused for CH2. */
			if (wfm_header.ModeTr1 == 2) fprintf(file, "unset arrow 3\n");
		}
		fprintf(file, "#\n");

		fprintf(file, "# Plot diagram CH2\n");
		fprintf(file, "plot \"%s.csv\" skip 2 using ($3*%u):($4*%u) with lines linecolor 2 notitle\n",
				file_name, X2con, Ch2con);
		fprintf(file, "#\n");

		/* Reset multiplot mode, just in case some output terminals relies on it. */
		fprintf(file, "unset multiplot");

		/* Leave function */
		goto exit;
	}

	/* Determine horizontal scaling for the plot */
	if ((wfm_header.TriggerMode == 4) && (wfm_header.CH2data == 1)) {
		fprintf(file, "# Horizontal scaling\n");
		fprintf(file, "set xlabel \"Time %s\" font \"Sans,16\"\n",
				(X2con == sec) ? "[s]" : (X2con == millisec) ? "[ms]" : (X2con == microsec) ? "[µs]" : "[ns]");
		fprintf(file, "set xrange [%g:%g]\n", XaxleCh2.min, XaxleCh2.max);
		fprintf(file, "set xtics out %g font \"Sans,14\" nomirror\n", XaxleCh2.incr);
		fprintf(file, "#\n");
	} else {
		fprintf(file, "# Horizontal scaling\n");
		fprintf(file, "set xlabel \"Time %s\" font \"Sans,16\"\n",
				(X1con == sec) ? "[s]" : (X1con == millisec) ? "[ms]" : (X1con == microsec) ? "[µs]" : "[ns]");
		fprintf(file, "set xrange [%g:%g]\n", XaxleCh1.min, XaxleCh1.max);
		fprintf(file, "set xtics out %g font \"Sans,14\" nomirror\n", XaxleCh1.incr);
		fprintf(file, "#\n");
	}

	/* Determine vertical scaling for the plot */
	if (wfm_header.CH1data == 1) {
		fprintf(file, "# Vertical scaling CH1\n");
		fprintf(file, "set ylabel \"Channel 1 %s\" font \"Sans,16\"\n", (Ch1con == volt) ? "[V]" : "[mV]");
		fprintf(file, "set yrange [%g:%g]\n", YaxleCh1.min, YaxleCh1.max);
		fprintf(file, "set ytics out %g font \"Sans,14\" nomirror\n", YaxleCh1.incr);
		fprintf(file, "#\n");
	}

	if (wfm_header.CH2data == 1) {
		/* If both CH1 and CH2 curve to be displayed, a second Y-axis is used for CH2 */
		fprintf(file, "# Vertical scaling CH2\n");
		fprintf(file, "set %s \"Channel 2 %s\" font \"Sans,16\"\n",
				(wfm_header.CH1data == 1) ? "y2label" : "ylabel", (Ch2con == volt) ? "[V]" : "[mV]");
		fprintf(file, "set %s [%g:%g]\n",
				(wfm_header.CH1data == 1) ? "y2range" : "yrange", YaxleCh2.min, YaxleCh2.max);
		fprintf(file, "set %s out %g font \"Sans,14\" nomirror\n",
				(wfm_header.CH1data == 1) ? "y2tics" : "ytics", YaxleCh2.incr);
		fprintf(file, "#\n");
	}

	/* In case of roll mode, no trigger lines are displayed */
	if (wfm_header.RollStpPt == 0) {
		/* CH1 data to be displayed and trigger source CH1 */
		if ((wfm_header.CH1data == 1) && (wfm_header.SourceTr1 == 0)) {
			fprintf(file, "# Trigger level and time instant CH1\n");
			fprintf(file, "set arrow from %g,%g to %g,%g size character 1.0,150 filled front\n",
					TrTimeInstCh1.x1, TrTimeInstCh1.y1, TrTimeInstCh1.x2, TrTimeInstCh1.y2);
			fprintf(file, "set arrow from %g,%g to %g,%g nohead front linetype 0 linecolor 1\n",
					TrLevel1Ch1.x1, TrLevel1Ch1.y1, TrLevel1Ch1.x2, TrLevel1Ch1.y2);
			/* In case of slope trigger mode add second trigger level line. */
			if (wfm_header.ModeTr1 == 2) {
				fprintf (file, "set arrow from %g,%g to %g,%g nohead front linetype 0 linecolor 1\n",
						TrLevel2Ch1.x1, TrLevel2Ch1.y1, TrLevel2Ch1.x2, TrLevel2Ch1.y2);
			}
			fprintf(file, "#\n");
		}
		/* CH2 data to be displayed and trigger source CH2 or alternate trigger mode set */
		if ((wfm_header.CH2data == 1) && ((wfm_header.SourceTr1 == 1) || (wfm_header.TriggerMode == 4))) {
			fprintf(file, "# Trigger level and time instant CH2\n");
			fprintf(file, "set arrow from %g,%s%g to %g,%s%g size character 1.0,150 filled front\n",
					TrTimeInstCh2.x1, (wfm_header.CH1data == 1) ? "second " : "", TrTimeInstCh2.y1,
					TrTimeInstCh2.x2, (wfm_header.CH1data == 1) ? "second " : "", TrTimeInstCh2.y2);
			fprintf(file, "set arrow from %g,%s%g to %g,%s%g nohead front linetype 0 linecolor 2\n",
					TrLevel1Ch2.x1, (wfm_header.CH1data == 1) ? "second " : "", TrLevel1Ch2.y1,
					TrLevel1Ch2.x2, (wfm_header.CH1data == 1) ? "second " : "", TrLevel1Ch2.y2);
			/* In case of slope trigger mode add second trigger level line. */
			if (((wfm_header.SourceTr1 == 1) && (wfm_header.ModeTr1 == 2)) ||
				((wfm_header.TriggerMode == 4) && (wfm_header.ModeTr2 == 2))) {
				fprintf (file, "set arrow from %g,%s%g to %g,%s%g nohead front linetype 0 linecolor 2\n",
						TrLevel2Ch2.x1, (wfm_header.CH1data == 1) ? "second " : "", TrLevel2Ch2.y1,
						TrLevel2Ch2.x2, (wfm_header.CH1data == 1) ? "second " : "", TrLevel2Ch2.y2);
			}
			fprintf(file, "#\n");
		}
	}

	/* Plot diagram */
	if ((wfm_header.CH1data == 1) && (wfm_header.CH2data == 1)) {
		/* Switch on legend in case CH1 and CH2 both are plotted into one diagram. */
		fprintf(file, "# Enable legend\n");
		fprintf(file, "# set key inside right top horizontal Left font \"Sans,16\"\n");
		fprintf(file, "set key inside right top vertical font \"Sans,16\"\n");
		fprintf(file, "#\n");

		fprintf(file, "# Plot diagram\n");
		fprintf(file, "plot \"%s.csv\" skip 2 using ($1*%u):($2*%u) with lines linecolor 1 axis x1y1 title \"CH1\", \\\n", file_name, X1con, Ch1con);
		fprintf(file, "     \"%s.csv\" skip 2 using ($1*%u):($3*%u) with lines linecolor 2 axis x1y2 title \"CH2\"\n", file_name, X1con, Ch2con);
	} else {
		fprintf(file, "# Plot diagram\n");
		if (wfm_header.CH1data == 1) {
			fprintf(file, "plot \"%s.csv\" skip 2 using ($1*%u):($2*%u) with lines linecolor 1 notitle\n",
					file_name, X1con, Ch1con);
		} else {
			fprintf(file, "plot \"%s.csv\" skip 2 using ($1*%u):($2*%u) with lines linecolor 2 notitle\n",
					file_name, (wfm_header.TriggerMode == 4) ? X2con : X1con, Ch2con);
		}
	}
exit:
	fclose(file);
	return 0;
error:
	return -1;
}


/**
 * Function that outputs *.wfm file oscilloscope settings into a *.txt file.
 *
 * The Function reads out oscilloscope settings of the *.wfm file header and
 * stores the adjustments in a *.txt file.
 *
 * @retval  0  No error.
 * @retval -1  Error while opening *.txt file.
 *
 * <b>Global variables</b>@n
 * @link wfm_file_name wfm_file_name @endlink <c>[in]</c>,
 * @link wfm_header    wfm_header    @endlink <c>[in]</c>
 */
int write_txt_file(void)
{
	char file_name[MAXSTRLEN];
	FILE *file;

	char *ActiveChannel[] = {"CH1","CH2","REF","MATH","LA"};
	char *TriggerMode[] = {"Edge", "Pulse", "Slope", "Video", "Alternate", "Pattern", "Duration"};
	char *TriggerSource[] = {"CH1", "CH2", "EXT", "AC Line", "D15-D0"};
	char *TriggerEdgeSlope[] = {"Rising", "Falling", "Rising & Falling"};
	char *TriggerPulseCond[] = {"Pos >", "Pos <", "Pos =", "Neg >", "Neg <", "Neg ="};
	char *TriggerSlopeCond[] = {"Rise >", "Rise <", "Rise =", "Fall >", "Fall <", "Fall ="};
	char *TriggerVideoSync[] = {"All Lines", "Line Num", "Odd Field", "Even Field"};
	char *TriggerSweep[] = {"Auto", "Normal", "Single"};
	char *TriggerCoupling[] = {"DC", "LF Reject", "HF Reject", "AC"};

	/* Open output file */
	strcpy(file_name, wfm_file_name);
	strcpy(strstr(file_name, ".wfm"), ".txt" );

	file = fopen(file_name, "w");
	if (file == NULL) {
		printf("Could not open file %s!\n", file_name);
		goto error;
	}

	/* General settings */
	fprintf(file, "General settings\n");
	fprintf(file, "================\n");
	fprintf(file, "File name                   : %s\n",  strrchr(wfm_file_name, '/') ? strrchr(wfm_file_name, '/')+1 : wfm_file_name);
	fprintf(file, "FileID                      : 0x%X\n", wfm_header.FileID);
	fprintf(file, "Channel 1 data stored       : %s\n", (wfm_header.CH1data == 1) ? "Y" : "N");
	fprintf(file, "Channel 2 data stored       : %s\n", (wfm_header.CH2data == 1) ? "Y" : "N");
	fprintf(file, "Logic analyzer data stored  : %s\n", ((wfm_header.LA1data == 1) || (wfm_header.LA2data == 1)) ? "Y": "N");
	fprintf(file, "Active Channel              : %s\n", ((wfm_header.ActiveCh > 0) && (wfm_header.ActiveCh <=5)) ? ActiveChannel[wfm_header.ActiveCh - 1] : "Unknown");
	fprintf(file, "Delayed scan mode           : %s\n", wfm_header.DelayedScan ? "On": "Off");
	if (wfm_header.TriggerMode == 4) {
		if (wfm_header.CH1data == 1)
			fprintf(file, "Data points channel 1       : %u\n", wfm_header.PtsPerCh);
		if (wfm_header.CH2data == 1)
			fprintf(file, "Data points channel 2       : %u\n", wfm_header.PtsCh2ATr);
		fprintf(file, "Trigger mode                : %s\n", (wfm_header.TriggerMode <=6) ? TriggerMode[wfm_header.TriggerMode] : "unknown");
	} else {
		fprintf(file, "Data points per channel     : %u\n", wfm_header.PtsPerCh);
		if (wfm_header.RollStpPt > 0) {
			fprintf(file, "Roll mode stop point        : %u\n", wfm_header.RollStpPt + 1);
		} else {
		fprintf(file, "Trigger mode                : %s\n", (wfm_header.TriggerMode <=6) ? TriggerMode[wfm_header.TriggerMode] : "unknown");
		}
	}
	fprintf(file, "\n");

	/* Channel 1 setup */
	if (wfm_header.CH1data == 1) {
		fprintf(file, "Channel 1 setup:\n");
		fprintf(file, "----------------\n");
		fprintf(file, "Probe attenuation           : %s\n", (wfm_header.ProbeCh1 == 0x3F80) ? "1x" : "10x");
		fprintf(file, "Vertical scale memory       : %gV/Div\n", (wfm_header.vScaleCh1M / 1e6) * ((wfm_header.ProbeCh1 == 0x3F80) ? 1 : 10));
		fprintf(file, "Vertical scale display      : %gV/Div\n", (wfm_header.vScaleCh1D / 1e6) * ((wfm_header.ProbeCh1 == 0x3F80) ? 1 : 10));
		fprintf(file, "Vertical offset memory      : %gV\n", ((wfm_header.vScaleCh1M / 1e6) * ((wfm_header.ProbeCh1 == 0x3F80) ? 1 : 10) * wfm_header.vPosCh1M) / 25);
		fprintf(file, "Vertical offset display     : %gV\n", ((wfm_header.vScaleCh1D / 1e6) * ((wfm_header.ProbeCh1 == 0x3F80) ? 1 : 10) * wfm_header.vPosCh1D) / 25);
		fprintf(file, "Memory data inverted        : %s\n", wfm_header.MemCh1Inv ? "Y" : "N");
		fprintf(file, "Display data inverted       : %s\n", wfm_header.DispCh1Inv ? "Y" : "N");
		fprintf(file, "\n");
	}

	/* Channel 2 setup */
	if (wfm_header.CH2data == 1) {
		fprintf(file, "Channel 2 setup:\n");
		fprintf(file, "----------------\n");
		fprintf(file, "Probe attenuation           : %s\n", (wfm_header.ProbeCh2 == 0x3F80) ? "1x" : "10x");
		fprintf(file, "Vertical scale memory       : %gV/Div\n", (wfm_header.vScaleCh2M / 1e6) * ((wfm_header.ProbeCh2 == 0x3F80) ? 1 : 10));
		fprintf(file, "Vertical scale display      : %gV/Div\n", (wfm_header.vScaleCh2D / 1e6) * ((wfm_header.ProbeCh2 == 0x3F80) ? 1 : 10));
		fprintf(file, "Vertical offset memory      : %gV\n", ((wfm_header.vScaleCh2M / 1e6) * ((wfm_header.ProbeCh2 == 0x3F80) ? 1 : 10) * wfm_header.vPosCh2M) / 25);
		fprintf(file, "Vertical offset display     : %gV\n", ((wfm_header.vScaleCh2D / 1e6) * ((wfm_header.ProbeCh2 == 0x3F80) ? 1 : 10) * wfm_header.vPosCh2D) / 25);
		fprintf(file, "Memory data inverted        : %s\n", wfm_header.MemCh2Inv ? "Y" : "N");
		fprintf(file, "Display data inverted       : %s\n", wfm_header.DispCh2Inv ? "Y" : "N");
		fprintf(file, "\n");
	}

	/* Logic analyzer setup */
	if ((wfm_header.LA1data == 1) || (wfm_header.LA2data == 1)) {
		fprintf(file, "Logic analyzer setup:\n");
		fprintf(file, "---------------------\n");
		fprintf(file, "Channels enabled D15-D8     :");
		if (!(wfm_header.LAchannels & 0xFF00)) fprintf(file, " None");
		if (wfm_header.LAchannels & 0x8000) fprintf(file, " D15");
		if (wfm_header.LAchannels & 0x4000) fprintf(file, " D14");
		if (wfm_header.LAchannels & 0x2000) fprintf(file, " D13");
		if (wfm_header.LAchannels & 0x1000) fprintf(file, " D12");
		if (wfm_header.LAchannels & 0x0800) fprintf(file, " D11");
		if (wfm_header.LAchannels & 0x0400) fprintf(file, " D10");
		if (wfm_header.LAchannels & 0x0200) fprintf(file, " D9");
		if (wfm_header.LAchannels & 0x0100) fprintf(file, " D8");
		fprintf(file, "\n");
		fprintf(file, "Channels enabled D7-D0      :");
		if (!(wfm_header.LAchannels & 0x00FF)) fprintf(file, " None");
		if (wfm_header.LAchannels & 0x0080) fprintf(file, " D7");
		if (wfm_header.LAchannels & 0x0040) fprintf(file, " D6");
		if (wfm_header.LAchannels & 0x0020) fprintf(file, " D5");
		if (wfm_header.LAchannels & 0x0010) fprintf(file, " D4");
		if (wfm_header.LAchannels & 0x0008) fprintf(file, " D3");
		if (wfm_header.LAchannels & 0x0004) fprintf(file, " D2");
		if (wfm_header.LAchannels & 0x0002) fprintf(file, " D1");
		if (wfm_header.LAchannels & 0x0001) fprintf(file, " D0");
		fprintf(file, "\n");
		fprintf(file, "Display size D15-D8         : %s\n", (wfm_header.DispSizeGrpLA2 == 7) ? "16ch" : "8ch");
		fprintf(file, "Display size D7-D0          : %s\n", (wfm_header.DispSizeGrpLA1 == 7) ? "16ch" : "8ch");
		if(wfm_header.LAchannels & 0x8000) fprintf(file, "Vertical position D15       : %u\n", wfm_header.vPosLaD15);
		if(wfm_header.LAchannels & 0x4000) fprintf(file, "Vertical position D14       : %u\n", wfm_header.vPosLaD14);
		if(wfm_header.LAchannels & 0x2000) fprintf(file, "Vertical position D13       : %u\n", wfm_header.vPosLaD13);
		if(wfm_header.LAchannels & 0x1000) fprintf(file, "Vertical position D12       : %u\n", wfm_header.vPosLaD12);
		if(wfm_header.LAchannels & 0x0800) fprintf(file, "Vertical position D11       : %u\n", wfm_header.vPosLaD11);
		if(wfm_header.LAchannels & 0x0400) fprintf(file, "Vertical position D10       : %u\n", wfm_header.vPosLaD10);
		if(wfm_header.LAchannels & 0x0200) fprintf(file, "Vertical position D9        : %u\n", wfm_header.vPosLaD9);
		if(wfm_header.LAchannels & 0x0100) fprintf(file, "Vertical position D8        : %u\n", wfm_header.vPosLaD8);
		if(wfm_header.LAchannels & 0x0080) fprintf(file, "Vertical position D7        : %u\n", wfm_header.vPosLaD7);
		if(wfm_header.LAchannels & 0x0040) fprintf(file, "Vertical position D6        : %u\n", wfm_header.vPosLaD6);
		if(wfm_header.LAchannels & 0x0020) fprintf(file, "Vertical position D5        : %u\n", wfm_header.vPosLaD5);
		if(wfm_header.LAchannels & 0x0010) fprintf(file, "Vertical position D4        : %u\n", wfm_header.vPosLaD4);
		if(wfm_header.LAchannels & 0x0008) fprintf(file, "Vertical position D3        : %u\n", wfm_header.vPosLaD3);
		if(wfm_header.LAchannels & 0x0004) fprintf(file, "Vertical position D2        : %u\n", wfm_header.vPosLaD2);
		if(wfm_header.LAchannels & 0x0002) fprintf(file, "Vertical position D1        : %u\n", wfm_header.vPosLaD1);
		if(wfm_header.LAchannels & 0x0001) fprintf(file, "Vertical position D0        : %u\n", wfm_header.vPosLaD0);
		fprintf(file, "Sample rate                 : %gHz\n", wfm_header.SampleRateLA);
		fprintf(file, "\n");
	}

	/* Time base setup */
	fprintf(file, "Time base setup:\n");
	fprintf(file, "----------------\n");
	if ((wfm_header.TriggerMode == 4) && (wfm_header.CH1data == 1)) {
		/* Setup for alternate trigger mode CH1 */
		fprintf(file, "CH1: Horizontal scale mem.  : %gs/Div\n", wfm_header.hScaleMT1 / 1e12);
		fprintf(file, "CH1: Horizontal scale disp. : %gs/Div\n", wfm_header.hScaleDT1 / 1e12);
		fprintf(file, "CH1: Horizontal offset mem. : %gs\n", wfm_header.hPosMT1 / 1e12);
		fprintf(file, "CH1: Horizontal offset disp.: %gs\n", wfm_header.hPosDT1 / 1e12);
		fprintf(file, "CH1: Sample rate            : %gHz\n", wfm_header.SampleRateT1);
		fprintf(file, "\n");
	}

	if ((wfm_header.TriggerMode == 4) && (wfm_header.CH2data == 1)) {
		/* Setup for alternate trigger mode CH2 */
		fprintf(file, "CH2: Horizontal scale mem.  : %gs/Div\n", wfm_header.hScaleMT2 / 1e12);
		fprintf(file, "CH2: Horizontal scale disp. : %gs/Div\n", wfm_header.hScaleDT2 / 1e12);
		fprintf(file, "CH2: Horizontal offset mem. : %gs\n", wfm_header.hPosMT2 / 1e12);
		fprintf(file, "CH2: Horizontal offset disp.: %gs\n", wfm_header.hPosDT2 / 1e12);
		fprintf(file, "CH2: Sample rate            : %gHz\n", wfm_header.SampleRateT2);
		fprintf(file, "\n");
	}

	if (wfm_header.TriggerMode != 4) {
		/* Setup for none alternate trigger mode */
		fprintf(file, "Horizontal scale memory     : %gs/Div\n", wfm_header.hScaleMT1 / 1e12);
		fprintf(file, "Horizontal scale display    : %gs/Div\n", wfm_header.hScaleDT1 / 1e12);
		fprintf(file, "Horizontal offset memory    : %gs\n", wfm_header.hPosMT1 / 1e12);
		fprintf(file, "Horizontal offset display   : %gs\n", wfm_header.hPosDT1 / 1e12);
		fprintf(file, "Sample rate                 : %gHz\n", wfm_header.SampleRateT1);
		fprintf(file, "\n");
	}

	/* In case of Roll Mode the trigger settings are ignored */
	if (wfm_header.RollStpPt > 0) goto exit;

	/* Trigger setup */
	fprintf(file, "Trigger setup:\n" );
	fprintf(file, "--------------\n" );

	/* Edge trigger mode */
	if (wfm_header.TriggerMode == 0) {
		fprintf(file, "Source                      : %s\n", (wfm_header.SourceTr1 <=4) ? TriggerSource[wfm_header.SourceTr1] : "unknown");
		fprintf(file, "Slope direction             : %s\n", (wfm_header.EdgeSlopeTr1 <=2) ? TriggerEdgeSlope[wfm_header.EdgeSlopeTr1] : "unknown");
		fprintf(file, "Trigger level               : %gV\n", wfm_header.LevelTr1);
		fprintf(file, "Sweep                       : %s\n", (wfm_header.SweepTr1 <= 2) ? TriggerSweep[wfm_header.SweepTr1] : "unknown");
		fprintf(file, "Coupling                    : %s\n", (wfm_header.CouplingTr1 <= 3) ? TriggerCoupling[wfm_header.CouplingTr1] : "unknown");
		fprintf(file, "Sensitivity                 : %g*Div\n", wfm_header.SensitivityTr1);
		fprintf(file, "Holdoff                     : %gs\n", wfm_header.HoldoffTr1);
	}

	/* Pulse trigger mode */
	if (wfm_header.TriggerMode == 1) {
		fprintf(file, "Source                      : %s\n", (wfm_header.SourceTr1 <=4) ? TriggerSource[wfm_header.SourceTr1] : "unknown");
		fprintf(file, "Pulse condition             : %s\n", (wfm_header.PulseCondTr1 <=5) ? TriggerPulseCond[wfm_header.PulseCondTr1] : "unknown");
		fprintf(file, "Pulse width                 : %gs\n", wfm_header.PulseWidthTr1);
		fprintf(file, "Trigger level               : %gV\n", wfm_header.LevelTr1);
		fprintf(file, "Sweep                       : %s\n", (wfm_header.SweepTr1 <= 2) ? TriggerSweep[wfm_header.SweepTr1] : "unknown");
		fprintf(file, "Coupling                    : %s\n", (wfm_header.CouplingTr1 <= 3) ? TriggerCoupling[wfm_header.CouplingTr1] : "unknown");
		fprintf(file, "Sensitivity                 : %g*Div\n", wfm_header.SensitivityTr1);
		fprintf(file, "Holdoff                     : %gs\n", wfm_header.HoldoffTr1);
	}

	/* Slope trigger mode */
	if (wfm_header.TriggerMode == 2) {
		fprintf(file, "Source                      : %s\n", (wfm_header.SourceTr1 <=2) ? TriggerSource[wfm_header.SourceTr1] : "unknown");
		fprintf(file, "Slope condition             : %s\n", (wfm_header.SlopeCondTr1 <=5) ? TriggerSlopeCond[wfm_header.SlopeCondTr1] : "unknown");
		fprintf(file, "Slope time                  : %gs\n", wfm_header.SlopeTimeTr1);
		fprintf(file, "Trigger level               : %gV\n", wfm_header.LevelTr1);
		fprintf(file, "Trigger level 2             : %gV\n", wfm_header.Level2Tr1);
		fprintf(file, "Sweep                       : %s\n", (wfm_header.SweepTr1 <= 2) ? TriggerSweep[wfm_header.SweepTr1] : "unknown");
		fprintf(file, "Coupling                    : %s\n", (wfm_header.CouplingTr1 <= 3) ? TriggerCoupling[wfm_header.CouplingTr1] : "unknown");
		fprintf(file, "Sensitivity                 : %g*Div\n", wfm_header.SensitivityTr1);
		fprintf(file, "Holdoff                     : %gs\n", wfm_header.HoldoffTr1);
	}

	/* Video trigger mode */
	if (wfm_header.TriggerMode == 3) {
		fprintf(file, "Source                      : %s\n", (wfm_header.SourceTr1 <= 2) ? TriggerSource[wfm_header.SourceTr1] : "unknown");
		fprintf(file, "Polarity                    : %s\n", (wfm_header.VideoPolTr1 == 0) ? "Pos" : "Neg");
		fprintf(file, "Sync                        : %s\n", (wfm_header.VideoSyncTr1 <= 3) ? TriggerVideoSync[wfm_header.VideoSyncTr1] : "unkown");
		fprintf(file, "Standard                    : %s\n", (wfm_header.VideoStdTr1 == 0) ? "NTSC" : "PAL/SECAM");
		fprintf(file, "Trigger level               : %gV\n", wfm_header.LevelTr1);
		fprintf(file, "Sweep                       : %s\n", (wfm_header.SweepTr1 <= 2) ? TriggerSweep[wfm_header.SweepTr1] : "unknown");
		fprintf(file, "Coupling (fixed)            : %s\n", (wfm_header.CouplingTr1 == 0) ? TriggerCoupling[wfm_header.CouplingTr1] : "unknown");
		fprintf(file, "Sensitivity                 : %g*Div\n", wfm_header.SensitivityTr1);
		fprintf(file, "Holdoff                     : %gs\n", wfm_header.HoldoffTr1);
	}

	/* Alternate trigger mode CH1 */
	if ((wfm_header.TriggerMode == 4) && (wfm_header.CH1data == 1)) {
		/* Channel 1 setup */
		fprintf(file, "CH1: Trigger mode           : %s\n", (wfm_header.ModeTr1 <=3) ? TriggerMode[wfm_header.ModeTr1] : "unknown");
		switch (wfm_header.ModeTr1) {
		case 0:
			/* Edge trigger mode */
			fprintf(file, "CH1: Slope direction        : %s\n", (wfm_header.EdgeSlopeTr1 <=1) ? TriggerEdgeSlope[wfm_header.EdgeSlopeTr1] : "unknown");
			fprintf(file, "CH1: Trigger level          : %gV\n", wfm_header.LevelTr1);
			break;
		case 1:
			/* Pulse trigger mode */
			fprintf(file, "CH1: Pulse condition        : %s\n", (wfm_header.PulseCondTr1 <=5) ? TriggerPulseCond[wfm_header.PulseCondTr1] : "unknown");
			fprintf(file, "CH1: Pulse width            : %gs\n", wfm_header.PulseWidthTr1);
			fprintf(file, "CH1: Trigger level          : %gV\n", wfm_header.LevelTr1);
			break;
		case 2:
			/* Slope trigger mode */
			fprintf(file, "CH1: Slope condition        : %s\n", (wfm_header.SlopeCondTr1 <=5) ? TriggerSlopeCond[wfm_header.SlopeCondTr1] : "unknown");
			fprintf(file, "CH1: Slope time             : %gs\n", wfm_header.SlopeTimeTr1);
			fprintf(file, "CH1: Trigger level          : %gV\n", wfm_header.LevelTr1);
			fprintf(file, "CH1: Trigger level 2        : %gV\n", wfm_header.Level2Tr1);
			break;
		case 3:
			/* Video trigger mode */
			fprintf(file, "CH1: Polarity               : %s\n", (wfm_header.VideoPolTr1 == 0) ? "Pos" : "Neg");
			fprintf(file, "CH1: Sync                   : %s\n", (wfm_header.VideoSyncTr1 <= 3) ? TriggerVideoSync[wfm_header.VideoSyncTr1] : "unkown");
			fprintf(file, "CH1: Standard               : %s\n", (wfm_header.VideoStdTr1 == 0) ? "NTSC" : "PAL/SECAM");
			fprintf(file, "CH1: Trigger level          : %gV\n", wfm_header.LevelTr1);
			break;
		}
		fprintf(file, "CH1: Coupling               : %s\n", (wfm_header.CouplingTr1 <= 3) ? TriggerCoupling[wfm_header.CouplingTr1] : "unknown");
		fprintf(file, "CH1: Sensitivity            : %g*Div\n", wfm_header.SensitivityTr1);
		fprintf(file, "CH1: Holdoff                : %gs\n", wfm_header.HoldoffTr1);
		fprintf(file, "\n");
	}

	/* Alternate trigger mode CH2 */
	if ((wfm_header.TriggerMode == 4) && (wfm_header.CH2data == 1)) {
		/* Channel 2 setup */
		fprintf(file, "CH2: Trigger mode           : %s\n", (wfm_header.ModeTr2 <=3) ? TriggerMode[wfm_header.ModeTr2] : "unknown");
		switch (wfm_header.ModeTr2) {
		case 0:
			/* Edge trigger mode */
			fprintf(file, "CH2: Slope direction        : %s\n", (wfm_header.EdgeSlopeTr2 <=2) ? TriggerEdgeSlope[wfm_header.EdgeSlopeTr2] : "unknown");
			fprintf(file, "CH2: Trigger level          : %gV\n", wfm_header.LevelTr2);
			break;
		case 1:
			/* Pulse trigger mode */
			fprintf(file, "CH2: Pulse condition        : %s\n", (wfm_header.PulseCondTr2 <=5) ? TriggerPulseCond[wfm_header.PulseCondTr2] : "unknown");
			fprintf(file, "CH2: Pulse width            : %gs\n", wfm_header.PulseWidthTr2);
			fprintf(file, "CH2: Trigger level          : %gV\n", wfm_header.LevelTr2);
			break;
		case 2:
			/* Slope trigger mode */
			fprintf(file, "CH2: Slope condition        : %s\n", (wfm_header.SlopeCondTr2 <=5) ? TriggerSlopeCond[wfm_header.SlopeCondTr2] : "unknown");
			fprintf(file, "CH2: Slope time             : %gs\n", wfm_header.SlopeTimeTr2);
			fprintf(file, "CH2: Trigger level          : %gV\n", wfm_header.LevelTr2);
			fprintf(file, "CH2: Trigger level 2        : %gV\n", wfm_header.Level2Tr2);
			break;
		case 3:
			/* Video trigger mode */
			fprintf(file, "CH2: Polarity               : %s\n", (wfm_header.VideoPolTr2 == 0) ? "Pos" : "Neg");;
			fprintf(file, "CH2: Sync                   : %s\n", (wfm_header.VideoSyncTr2 <= 3) ? TriggerVideoSync[wfm_header.VideoSyncTr2] : "unkown");
			fprintf(file, "CH2: Standard               : %s\n", (wfm_header.VideoStdTr2 == 0) ? "NTSC" : "PAL/SECAM");
			fprintf(file, "CH2: Trigger level          : %gV\n", wfm_header.LevelTr2);
			break;
		}
		fprintf(file, "CH2: Coupling               : %s\n", (wfm_header.CouplingTr2 <= 3) ? TriggerCoupling[wfm_header.CouplingTr2] : "unknown");
		fprintf(file, "CH2: Sensitivity            : %g*Div\n", wfm_header.SensitivityTr2);
		fprintf(file, "CH2: Holdoff                : %gs\n", wfm_header.HoldoffTr2);
		fprintf(file, "\n");
	}

	/* Pattern trigger mode */
	if (wfm_header.TriggerMode == 5) {
		fprintf(file, "Pattern trigger mode not supported yet.\n");
	}

	/* Duration trigger mode */
	if (wfm_header.TriggerMode == 6) {
		fprintf(file, "Duration trigger mode not supported yet.\n");
	}

exit:
	fclose(file);
	return 0;
error:
	return -1;
}


/**
 * Function that outputs *.wfm file measurement data into a *.wav file.
 *
 * The Function reads out and converts the measurement data of the *.wfm file
 * and writes it into a *.wav file which can be used afterwards by an audio
 * editor to investigate the curve progression over time in more detail.
 * X-values will be stored without any modifications as sample points, Y-values,
 * however, will be stored as pixels shown on the oscilloscope screen with respect
 * to the vertical scale and position setting of the oscilloscope.
 *
 * @retval  0  No error.
 * @retval -1  Error while opening *.cvs file, memory allocation or writing
 *             *.wav file header.
 *
 * <b>Global variables</b>@n
 * @link wfm_file_name wfm_file_name @endlink <c>[in]</c>,
 * @link wfm_file      wfm_file      @endlink <c>[in]</c>,
 * @link wfm_header    wfm_header    @endlink <c>[in]</c>
 */
int write_wav_file()
{
	char file_name[MAXSTRLEN];
	FILE *file;

	RiffWave_t wav_header;

	struct {int16_t ch1; int16_t ch2;} *data;
	uint32_t i, numpts;

	/* Determine number of data points */
	if (wfm_header.RollStpPt > 0) numpts = wfm_header.RollStpPt + 1;
	else numpts = wfm_header.PtsPerCh;

	/* Allocte memory */
	data = calloc(sizeof(*data), numpts);
	if (data == NULL){
		printf("Memory allocation failed!\n");
		goto error;
	}

	/* -----------------------------
	 * Calculation of Ch1/Ch2 values
	 * -----------------------------
	 * 1. If wfm data is stored non inverted:
	 *       wav data = (screen offset - wfm data - vertical position (mem.)) [pixel]
	 *    If wfm data is stored inverted:
	 *       wav data = (wfm data - vertical position (mem.) - screen offset) [pixel]
	 * 2. If scale is changed after measurement (display scale != memory scale):
	 *       wav data = wav data * memory scale / display scale
	 * 3. If the polarity is changed after measurement (display data inverted != memory data inverted)
	 *       wav data = (wave file offset - wav data + vertical position (disp.)) [pixel]
	 *    If the polarity is not changed after measurement:
	 *       wav data = (wav data - vertical position (disp.) + wave file offset) [pixel]
	 * 4. If wav data exceeds the range of unit8_t it will be limited to uint8_t boundaries finally.
	 */

	/* If channel 1 data are available, read in and prepare CH1 data into memory */
	if (wfm_header.CH1data == 1) {
		/* Set *.wfm file pointer to the beginning of CH1 data */
		fseek(wfm_file, sizeof(wfm_header), SEEK_SET);

		/* Prepare CH1 data with respect of inverted storage and scaling */
		if (wfm_header.MemCh1Inv == 0) {
			for (i = 0; i < numpts; i++)
				data[i].ch1 = 125 - fgetc(wfm_file) - wfm_header.vPosCh1M;
		} else {
			for (i = 0; i < numpts; i++)
				data[i].ch1 = fgetc(wfm_file) - wfm_header.vPosCh1M - 125;
		}
		if (wfm_header.vScaleCh1D != wfm_header.vScaleCh1M) {
			for (i = 0; i < numpts; i++)
				data[i].ch1 = (int64_t)data[i].ch1 * wfm_header.vScaleCh1M / wfm_header.vScaleCh1D;
		}
		if (wfm_header.DispCh1Inv != wfm_header.MemCh1Inv) {
			for (i = 0; i < numpts; i++)
				data[i].ch1 = 128 - data[i].ch1 + wfm_header.vPosCh1D;
		} else {
			for (i = 0; i < numpts; i++)
				data[i].ch1 = data[i].ch1 + wfm_header.vPosCh1D + 128;
		}
		for (i = 0; i < numpts; i++) {
			if (data[i].ch1 > 255) data[i].ch1 = 255;
			else if (data[i].ch1 < 0) data[i].ch1 = 0;
		}
	}

	/* If channel 2 data are available, read in and prepare CH2 data into memory */
	if (wfm_header.CH2data == 1) {
		/* Set *.wfm file pointer to the beginning of CH2 data */
		if (wfm_header.CH1data == 1) {
			fseek(wfm_file, (sizeof(wfm_header) + wfm_header.PtsPerCh), SEEK_SET);
		} else {
			fseek(wfm_file, sizeof(wfm_header), SEEK_SET);
		}

		/* Prepare CH2 data with respect of inverted storage and scaling */
		if (wfm_header.MemCh2Inv == 0) {
			for (i = 0; i < numpts; i++)
				data[i].ch2 = 125 - fgetc(wfm_file) - wfm_header.vPosCh2M;
		} else {
			for (i = 0; i < numpts; i++)
				data[i].ch2 = fgetc(wfm_file) - wfm_header.vPosCh2M - 125;
		}
		if (wfm_header.vScaleCh2D != wfm_header.vScaleCh2M) {
			for (i = 0; i < numpts; i++)
				data[i].ch2 = (int64_t)data[i].ch2 * wfm_header.vScaleCh2M / wfm_header.vScaleCh2D;
		}
		if (wfm_header.DispCh2Inv != wfm_header.MemCh2Inv) {
			for (i = 0; i < numpts; i++)
				data[i].ch2 = 128 - data[i].ch2 + wfm_header.vPosCh2D;
		} else {
			for (i = 0; i < numpts; i++)
				data[i].ch2 = data[i].ch2 + wfm_header.vPosCh2D + 128;
		}
		for (i = 0; i < numpts; i++) {
			if (data[i].ch2 > 255) data[i].ch2 = 255;
			else if (data[i].ch2 < 0) data[i].ch2 = 0;
		}
	}

	/* Open output file */
	strcpy(file_name, wfm_file_name);
	strcpy(strstr(file_name, ".wfm"), ".wav" );

	file = fopen(file_name, "w");
	if (file == NULL) {
		printf("Could not open %s!\n", file_name);
		goto error;
	}

	/* Prepare wave file header */

	/* data subchunk */
	wav_header.DataID[0] = 'd';
	wav_header.DataID[1] = 'a';
	wav_header.DataID[2] = 't';
	wav_header.DataID[3] = 'a';
	wav_header.DataSize  = (wfm_header.CH1data == 1) ? numpts : 0;
	wav_header.DataSize += (wfm_header.CH2data == 1) ? numpts : 0;

	/* fmt subchunk */
	wav_header.FmtID[0]      = 'f';
	wav_header.FmtID[1]      = 'm';
	wav_header.FmtID[2]      = 't';
	wav_header.FmtID[3]      = ' ';
	wav_header.FmtSize       = 16;		// size of fmt chunk without FmtID and FmtSize
	wav_header.FmtTag        = 0x0001;	// 0x0001 := PCM
	wav_header.Channels      = (wfm_header.CH1data == 1) ? 1 : 0;
	wav_header.Channels     += (wfm_header.CH2data == 1) ? 1 : 0;
	wav_header.SampleRate    = numpts;	// period of audio track always 1s
	wav_header.BitsPerSample = 8;
	wav_header.FrameSize     = wav_header.Channels * ((wav_header.BitsPerSample + 7) / 8);
	wav_header.BytesPerSec   = wav_header.FrameSize * wav_header.SampleRate;

	/* RIFF chunk */
	wav_header.ID[0]   = 'R';
	wav_header.ID[1]   = 'I';
	wav_header.ID[2]   = 'F';
	wav_header.ID[3]   = 'F';
	wav_header.Size    =  4;			//   size of RIFF chunk without ID and Size
	wav_header.Size   += 24 ;			// + size of fmt subchunk
	wav_header.Size   +=  8 + wav_header.DataSize;	// + size of data subchunk
	wav_header.Type[0] = 'W';
	wav_header.Type[1] = 'A';
	wav_header.Type[2] = 'V';
	wav_header.Type[3] = 'E';

	/* Write *.wav file header */
	if (fwrite(&wav_header, sizeof(wav_header), 1, file) != 1) {
		printf("Could not write wave file header to %s!\n", file_name);
		fclose(file);
		goto error;
	}

	/* Write *.wav file data */
	if ((wfm_header.CH1data == 1) && (wfm_header.CH2data == 1)) {
		/* Write data of CH1 and CH2 */
		for (i = 0; i < numpts; i++) {
			fputc((char)data[i].ch1, file);
			fputc((char)data[i].ch2, file);
		}
	} else {
		/* Write data of CH1 or CH2 */
		if (wfm_header.CH1data == 1) {
			for (i = 0; i < numpts; i++)
				fputc((char)data[i].ch1, file);
		} else {
			for (i = 0; i < numpts; i++)
				fputc((char)data[i].ch2, file);
		}
	}

	fclose(file);
	free(data);
	return 0;
error:
	free(data);
	return -1;
}


/**
 * Main function of the program.
 *
 * The main function controls the program flow as follows:
 * 1. The program invocation is checked and in case of none error,
 * 2. every committed *.wfm-file is checked and in case of none error,
 * 3. a CSV file is written if requested, and/or
 * 4. a gnuplot script file is written if requested, and/or
 * 5. a info file is written if requested, and/or
 * 6. a RIFF-WAVE file is written if requested.
 *
 * @param[in]  argc  argument counter
 * @param[in]  argv  argument vector
 *
 * @retval  EXIT_SUCCESS  No error.
 * @retval  EXIT_FILURE   Error while program invocation or opening a *.wfm file.
 *
 *
 * <b>Global variables</b>@n
 * @link wfm_file_name wfm_file_name @endlink <c>[in, out]</c>,
 * @link wfm_file      wfm_file      @endlink <c>[in, out]</c>,
 * @link wfm_header    wfm_header    @endlink <c>[out]</c>
 * @link write_csv     write_csv     @endlink <c>[in]</c>,
 * @link write_gp      write_gp      @endlink <c>[in]</c>,
 * @link write_txt     write_txt     @endlink <c>[in]</c>,
 * @link write_wav     write_wav     @endlink <c>[in]</c>,
 */
int main(int argc, char *argv[])
{
	int i;

	/* Check program invocation */
	i = check_invocation(argc, argv);
	if (i == 0) goto exit;
	if (i == -1) goto error;

	/* For every *.wfm file given: */
	do {
		/* Prepare 'wfm_file_name' and open 'wfm_file' for global access. */
		strcpy(wfm_file_name, argv[i]);
		wfm_file = fopen(wfm_file_name, "r");
		if (wfm_file == NULL) {
			printf("Could not open %s!\n", wfm_file_name);
		} else {
			/* Read in file header and provide 'wfm_header' for global access. */
			if (fread(&wfm_header, sizeof(wfm_header), 1, wfm_file) != 1) {
				printf("Could not read file header of %s!\n", wfm_file_name);
			} else {
				/* Check wfm file header */
				if (check_wfm_file() == 0) {
					/* If requested, write measurement data into CSV file. */
					if (write_csv) write_csv_file();

					/* If requested, generate gnuplot script file. */
					if (write_gp) write_gp_file();

					/* If requested, output wfm header data into text file. */
					if (write_txt) write_txt_file();

					/* If requested, write measurement data into RIFF-WAVE file. */
					if (write_wav) write_wav_file();
				}
			}
			fclose (wfm_file);
		}
	} while (++i < argc);

exit:
	return EXIT_SUCCESS;
error:
	return EXIT_FAILURE;
}
